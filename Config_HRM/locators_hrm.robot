*** Variables ***

#URL common variable
${Browser}   firefox
${SiteURL}    https://opensource-demo.orangehrmlive.com/web/index.php/auth/login   

#Login page variables
${user}    Admin 
${pass}    admin123

#PIM page variables
${fname}    sujata
${mname}    some
${lname}    mohandule
${empid}    2424


#recruitment variables
${can_fname}    prayan
${can_mname}    pramod
${can_lname}    gawari
${can_emailaddress}    test@gmail.com 

#Login page locators 
${username}    //input[@name='username']  
${password}    //input[@name='password'] 
${loginbtn}    //button[@type='submit']

#pim page locators
${PIM}    //*[@id="app"]/div[1]/div[1]/aside/nav/div[2]/ul/li[2]/a/span
${addpim}    //*[@id="app"]/div[1]/div[2]/div[2]/div/div[2]/div[1]/button
${firstname}    //input[@name="firstName"]
${middlename}    //input[@name="middleName"]
${lastname}    //input[@name="lastName"]
${employeeid}    //*[@id="app"]/div[1]/div[2]/div[2]/div/div/form/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/input
${pimsave}    //button[@type="submit"]


#Recruitment
${recruitment}    //*[@id="app"]/div[1]/div[1]/aside/nav/div[2]/ul/li[5]/a
${recuitment_add}    //*[@id="app"]/div[1]/div[2]/div[2]/div/div[2]/div[1]/button
${can_firstname}    //input[@name="firstName"]
${can_middlename}    //input[@name="middleName"]
${canlastname}    //input[@name="lastName"]
${can_email}    //*[@id="app"]/div[1]/div[2]/div[2]/div/div/form/div[3]/div/div[1]/div/div[2]/input
${recruite_save}    //button[@type='submit']
 