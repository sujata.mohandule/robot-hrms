*** Settings ***
Library    SeleniumLibrary 
Resource    ../utilities_HRM/login_keywords.robot
Resource    ../utilities_HRM/admin_keywords.robot 
Resource    ../utilities_HRM/recruitment_keywords.robot  


*** Variables ***
${HC}    headlesschrome

*** Test Cases ***

login to orangehrm
    [Documentation]    Login to orangeHRM
    [Tags]    t1
    open my browser 
    Apply implicit wait    
    Enter username 
    Sleep    5  
    Enter password  
    Sleep    5  
    Click on login 
    Sleep    5  

PIM details page
    Click on PIM tab
    Sleep    5    
    Click on add button
    Sleep    3 
    Enter first name
    Sleep    3 
    Enter middle name
    Sleep    3 
    Enter last name
    Sleep    3 
    Enter employee id
    Sleep    3 
    Click on PIM save  
    Sleep    5

 
recruitment details
    click on recruitment tab
    Sleep    3    
    click on recruitment add button
    Sleep    3    
    enter candidate firstname
    Sleep    3    
    enter candidate middlename
    Sleep    3    
    enter candidate lastname
    Sleep    3    
    enter candidate email address
    Sleep    3    
    click on recruitment save button
    Sleep    5    





   
