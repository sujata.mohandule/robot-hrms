*** Settings ***
Library    SeleniumLibrary 
Resource    ../Config_HRM/locators_hrm.robot

*** Keywords ***

#PIM details tab
Click on PIM tab
    Click Element    ${PIM}
Click on add button
    Click Element    ${addpim}   
Enter first name
    Input Text    ${firstname}    ${fname}                
Enter middle name
    Input Text    ${middlename}    ${mname}
Enter last name
    Input Text    ${lastname}    ${lname}
Enter employee id
    Input Text    ${employeeid}    ${empid}       
Click on PIM save
    Click Element    ${pimsave}     
    
         