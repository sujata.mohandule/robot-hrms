*** Settings ***
Library    SeleniumLibrary 
Resource    ../Config_HRM/locators_hrm.robot

*** Keywords ***

open my browser
    Open Browser    ${SiteURL}    ${Browser}
    Maximize Browser Window
    
Apply implicit wait
    ${implicittime}=    Get Selenium Implicit Wait
    Log To Console    ${implicittime} 
    Set Selenium Implicit Wait    10 seconds
    
Enter username
    Input Text    ${username}    ${user}   
    
Enter password 
    Input Text    ${password}    ${pass}    
    
Click on login
    Click Element    ${loginbtn}    
